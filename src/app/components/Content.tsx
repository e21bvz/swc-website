import React from 'react';
import { Autoplay, EffectCoverflow } from 'swiper/modules';
import { Swiper, SwiperSlide } from 'swiper/react';
import StackIcon from 'tech-stack-icons';

const data = [
  {
    type: 'api',
    title: 'point-exchange',
    achitecture: 'microservice',
    link: 'https://github.com/Nack-thanaphon/point-exchange.git',
    techStack: [
      <StackIcon className='m-1 w-[30px]' name='go' key='java1' />,
      <StackIcon className='m-1 w-[30px]' name='postman' key='java2' />,
      <StackIcon className='m-1 w-[30px]' name='docker' key='java2' />,
    ],
  },
  {
    type: 'api',
    title: 'webhook-service',
    link: 'https://github.com/Nack-thanaphon/webhook-service',
    techStack: [
      <StackIcon className='m-1 w-[30px]' name='nodejs' key='nodejs' />,
      <StackIcon className='m-1 w-[30px]' name='postman' key='java2' />,
      <StackIcon className='m-1 w-[30px]' name='js' key='js' />,
    ],
  },
  {
    type: 'api',
    title: 'payment-processing',
    link: 'data-transfers',
    techStack: [
      <StackIcon className='m-1 w-[30px]' name='postman' key='java2' />,
      <StackIcon className='m-1 w-[30px]' name='nodejs' key='nodejs' />,
      <StackIcon className='m-1 w-[30px]' name='typescript' key='java2' />,
    ],
  },
  {
    type: 'api',
    title: 'load-test',
    link: 'data-transfers',
    techStack: [
      <StackIcon className='m-1 w-[30px]' name='go' key='java1' />,
      <StackIcon className='m-1 w-[30px]' name='postman' key='java2' />,
    ],
  },
  {
    type: 'web-application',
    title: 'Booking-app',
    link: 'data-transfers',
    techStack: [
      <StackIcon className='m-1 w-[30px]' name='angular' key='angular' />,
      <StackIcon className='m-1 w-[30px]' name='java' key='java1' />,
      <StackIcon className='m-1 w-[30px]' name='postman' key='java2' />,
    ],
  },
  {
    type: 'web-application',
    title: 'Movie-app',
    link: 'data-transfers',
    techStack: [
      <StackIcon className='m-1 w-[30px]' name='laravel' key='java1' />,
      <StackIcon className='m-1 w-[30px]' name='docker' key='java2' />,
      <StackIcon className='m-1 w-[30px]' name='mysql' key='java2' />,
    ],
  },
  {
    type: 'web-application',
    title: 'cms',
    link: 'data-transfers',
    techStack: [
      <StackIcon className='m-1 w-[30px]' name='cakephp' key='java1' />,
      <StackIcon className='m-1 w-[30px]' name='docker' key='java2' />,
      <StackIcon className='m-1 w-[30px]' name='mysql' key='java2' />,
    ],
  },
  {
    type: 'web-application',
    title: 'Mini-store',
    link: 'data-transfers',
    techStack: [
      <StackIcon className='m-1 w-[30px]' name='angular' key='java1' />,
      <StackIcon className='m-1 w-[30px]' name='nestjs' key='java2' />,
      <StackIcon className='m-1 w-[30px]' name='postman' key='java2' />,
    ],
  },
  {
    type: 'web-application',
    title: 'Pos',
    link: 'data-transfers',
    techStack: [
      <StackIcon className='m-1 w-[30px]' name='nuxtjs' key='java1' />,
      <StackIcon className='m-1 w-[30px]' name='nestjs' key='java2' />,
    ],
  },
  {
    type: 'web-application',
    title: 'PopClick',
    link: 'data-transfers',
    techStack: [
      <StackIcon className='m-1 w-[30px]' name='reactjs' key='java1' />,
      <StackIcon className='m-1 w-[30px]' name='nestjs' key='java2' />,
      <StackIcon className='m-1 w-[30px]' name='mongodb' key='java2' />,
    ],
  },
  {
    type: 'web-application',
    title: 'Queue-app ',
    link: 'data-transfers',
    techStack: [
      <StackIcon className='m-1 w-[30px]' name='go' key='java1' />,
      <StackIcon className='m-1 w-[30px]' name='kafka' key='java2' />,
      <StackIcon className='m-1 w-[30px]' name='kubernetes' key='java2' />,
      <StackIcon className='m-1 w-[30px]' name='mongodb' key='java2' />,
    ],
  },
];

const Content = () => {
  return (
    <>
      <Swiper
        slidesPerView={3}
        spaceBetween={30}
        // pagination={{ clickable: true }}
        autoplay={{
          delay: 2500,
          disableOnInteraction: false,
        }}
        modules={[EffectCoverflow, Autoplay]}
        breakpoints={{
          // when window width is >= 320px
          320: {
            slidesPerView: 1,
            spaceBetween: 10,
          },
          // when window width is >= 640px
          640: {
            slidesPerView: 2,
            spaceBetween: 20,
          },
          // when window width is >= 1024px
          1024: {
            slidesPerView: 3,
            spaceBetween: 1,
          },
        }}
      >
        {data.map((item, index) => (
          <SwiperSlide key={index}>
            <div className='text-black bg-white border p-4  sm:w-[400px] h-auto rounded-[10px]  '>
              <div className='mb-3'>
                <img
                  src='https://cdn.shopify.com/app-store/listing_images/e3639682dd96fcb7ec291e8af21a37d1/desktop_screenshot/CLegu5LLgIIDEAE=.png?height=720&width=1280'
                  className='w-full rounded-[10px]'
                  alt='API'
                />
              </div>
              <div className='bg-white p-3 rounded-[10px]'>
                <p className='capitalize mb-2'>
                  <span className='text-white bg-blue-800 px-4 py-1 rounded-[20px] uppercase'>
                    {item.type}
                  </span>
                </p>
                <p className='uppercase'>name: </p>
                <h3 className='text-blue-400 font-bold capitalize'>
                  {item.title}
                </h3>
                <p className='uppercase mb-3'>tech-stack: </p>
                <div className='flex my-1'>
                  {item.techStack.map((tech, techIndex) => (
                    <React.Fragment key={techIndex}>{tech}</React.Fragment>
                  ))}
                </div>
              </div>
              <div className='mt-[10px] flex justify-end  my-auto'>
                <a
                  href={item.link}
                  className='rounded-[5px]  text-blue my-auto border   p-[10px]'
                >
                  <div className='flex'>
                    <StackIcon
                      className='w-[20px] m-0 p-0 mr-4'
                      name='github'
                    />
                    <span className='my-auto'>View on Github</span>
                  </div>
                </a>
              </div>
            </div>
          </SwiperSlide>
        ))}
      </Swiper>
    </>
  );
};

export default Content;
