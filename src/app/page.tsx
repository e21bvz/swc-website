'use client';

import gsap from 'gsap';
import { ScrollToPlugin } from 'gsap/ScrollToPlugin';
import * as React from 'react';
import '@/lib/env';

gsap.registerPlugin(ScrollToPlugin);
/**
 * SVGR Support
 * Caveat: No React Props Type.
 *
 * You can override the next-env if the type is important to you
 * @see https://stackoverflow.com/questions/68103844/how-to-override-next-js-svg-module-declaration
 */

import StackIcon from 'tech-stack-icons';

import Footer from '@/app/components/Footer';

import Profile from './components/Profile';

const scrollToSection = (id: string) => {
  gsap.to(window, { duration: 0.3, scrollTo: `#${id}` });
};
// type Color = (typeof colorList)[number];

export default function HomePage() {
  // const [mode, setMode] = React.useState<'dark' | 'light'>('light');
  // const [color, setColor] = React.useState<Color>('sky');

  // function toggleMode() {
  //   setMode(mode === 'dark' ? 'light' : 'dark');
  // }

  return (
    <main
      className='bg-scroll bg-cover bg-center h-full backdrop-blur-3xl'
      style={{ backgroundImage: `url('images/19366.jpg')` }}
    >
      <section>
        <section className='h-full pt-6'>
          <div className='text-center sm:flex justify-between w-full h-screen sm:p-[40px]'>
            <div className='sm:w-2/4 w-full my-auto p-2'>
              <div className='sm:leading-[70px] leading-[40px]'>
                <p className='text-blue-400 sm:text-[2rem] font-bold'>
                  Welcome to
                </p>
                <p className='sm:text-[5rem] text-[2rem] font-bold'>
                  contactus.work
                </p>
                <p className='sm:text-[2rem] text-slate-300'>
                  Perfectly crafted for your business.
                </p>
                <p>Digital Transformation and Tech Solutions</p>
              </div>
              <div className='flex justify-center mt-4'>
                <a
                  href='https://github.com/Nack-thanaphon'
                  target='_blank'
                  className='bg-blue-500 text-white mr-2 p-3 px-4 rounded-[10px]'
                >
                  <p className='font-bold'>Our Projects</p>
                  {/* <small>Link to service profile</small> */}
                </a>
                <button
                  className='border text-black mr-2 p-3 px-4 rounded-[10px]'
                  onClick={() => scrollToSection('engineer')}
                >
                  <p className='font-bold'>Hi,Nack 👋</p>
                  {/* <small>Link to service profile</small> */}
                </button>
              </div>
            </div>
            <div className='sm:flex w-full rounded-[10px] py-[50px] my-auto'>
              <Profile />
            </div>
          </div>
          {/* <div
            className='sm:flex p-2 sm:px-[80px]  w-full sm:h-screen my-[10px]'
            id='engineer'
          >
            <div className='sm:w-2/4  mb-4 '>
              <div className='leading-[40px] sm:leading-[70px]'>
                <p className='sm:text-[4rem] text-[2rem] font-bold'>
                  Our Team{' '}
                </p>
                <p className='sm:text-[1.5rem] text-slate-300'>
                  Perfesional skill and responsibility
                </p>
              </div>
            </div>
            <div className='sm:flex w-full rounded-[10px] py-[50px] my-auto'>
              <Profile />
            </div>
          </div> */}
          <div className=' p-2 sm:p-[80px] w-full  my-[10px]' id='tech'>
            <div className='sm:w-2/4 leading-[40px] sm:leading-[70px]'>
              <p className='sm:text-[4rem] text-[2rem] font-bold'>
                Tech-Stack{' '}
              </p>
              <p className='sm:text-[1.5rem] text-slate-300'>
                perfectly crafted for your business, contactus.work{' '}
              </p>
            </div>
            <div className=' shadow-lg w-full rounded-[10px] p-[35px]  bg-white py-[50px]'>
              <div className='my-2'>
                <p>Frontend</p>
              </div>
              <div className='text-white grid  grid-cols-10 gap-5 mb-[30px]'>
                <StackIcon className='lg:w-20 w-5' name='nextjs2' />
                <StackIcon className='lg:w-20 w-5' name='tailwindcss' />
                <StackIcon className='lg:w-20 w-5' name='bootstrap5' />
                <StackIcon className='lg:w-20 w-5' name='html5' />
                <StackIcon className='lg:w-20 w-5' name='jquery' />
                <StackIcon className='lg:w-20 w-5' name='js' />
                <StackIcon className='lg:w-20 w-5' name='materialui' />
                <StackIcon className='lg:w-20 w-5' name='typescript' />
                <StackIcon className='lg:w-20 w-5' name='swift' />
              </div>

              <div className='my-2'>
                <p>Backend</p>
              </div>
              <div className='text-white grid  grid-cols-10 gap-5 mb-[30px]'>
                <StackIcon className='lg:w-20 w-5' name='laravel' />
                <StackIcon className='lg:w-20 w-5' name='codeigniter' />
                <StackIcon className='lg:w-20 w-5' name='elastic' />
                <StackIcon className='lg:w-20 w-5' name='mongodb' />
                <StackIcon className='lg:w-20 w-5' name='mysql' />
                <StackIcon className='lg:w-20 w-5' name='nestjs' />
                <StackIcon className='lg:w-20 w-5' name='postgresql' />
                <StackIcon className='lg:w-20 w-5' name='java' />
                <StackIcon className='lg:w-20 w-5' name='linux' />
                <StackIcon className='lg:w-20 w-5' name='ubuntu' />
              </div>
              <div className='my-2'>
                <p>CI/CD & </p>
              </div>
              <div className='text-white grid  grid-cols-10 gap-5 mb-[30px]'>
                <StackIcon className='lg:w-20 w-5' name='firebase' />
                <StackIcon className='lg:w-20 w-5' name='docker' />
                <StackIcon className='lg:w-20 w-5' name='digitalocean' />
                <StackIcon className='lg:w-20 w-5' name='gitlab' />
                <StackIcon className='lg:w-20 w-5' name='kubernetes' />
                <StackIcon className='lg:w-20 w-5' name='netlify' />
                <StackIcon className='lg:w-20 w-5' name='playwright' />
                <StackIcon className='lg:w-20 w-5' name='postman' />
                <StackIcon className='lg:w-20 w-5' name='jest' />
              </div>
              <div className='my-2'>
                <p>Utility</p>
              </div>
              <div className='text-white  grid  grid-cols-10 gap-5 mb-[30px]'>
                <StackIcon className='lg:w-20 w-5' name='atlassian' />
                <StackIcon className='lg:w-20 w-5' name='jira' />
                <StackIcon className='lg:w-20 w-5' name='figma' />
                <StackIcon className='lg:w-20 w-5' name='slack' />
              </div>
            </div>
          </div>

          {/* web design (3d 2 ตัว webdesign clean lading page)
          api document (docment image capture postman)
          web development (CMS Calculate)
          mobile development (react-native firebase)
          project integration (microservice cicd show case) */}

          {/* <div
            className=' p-2 sm:p-[80px] w-full sm:h-screen my-[10px]'
            id='job'
          >
            <div className='sm:w-2/4 leading-[40px] sm:leading-[70px]'>
              <p className='sm:text-[4rem] text-[2rem] font-bold'>
                Participate
              </p>
              <p className='sm:text-[1.5rem] text-slate-300'>
                Push your future with contactus.work
              </p>
            </div>
            <div className=' shadow-lg w-full rounded-[10px] p-[30px]  bg-white py-[50px]'>
              <div className='text-white grid  grid-cols-6 gap-5 mb-[30px]'></div>
            </div>
          </div> */}
          {/* <div
            className=' p-2 sm:p-[80px] w-full sm:h-screen my-[10px]'
            id='job'
          >
            <div className='sm:w-2/4 leading-[40px] sm:leading-[70px]'>
              <p className='sm:text-[4rem] text-[2rem] font-bold'>
                Participate
              </p>
              <p className='sm:text-[1.5rem] text-slate-300'>
                Push your future with contactus.work
              </p>
            </div>
            <div className=' shadow-lg w-full rounded-[10px] p-[30px]  bg-white py-[50px]'>
              <div className='text-white grid  grid-cols-6 gap-5 mb-[30px]'></div>
            </div>
          </div> */}
        </section>
        <Footer />
      </section>
    </main>
  );
}
