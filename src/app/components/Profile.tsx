import React from 'react';

const Profile = () => {
  return (
    <>
      <div className='text-center m-sm-[20px] mx-auto mb-[40px]'>
        <img
          alt='Description'
          className='sm:w-[300px] sm:h-[300px] w-[150px] h-[150px] cover overflow-hidden rounded-full mx-auto my-[10px]'
          src='images/profile.jpg'
        />
        <h4 className='text-blue-400 sm:text-[2.2rem] font-bold  leading-relaxed text-nowrap'>
          Thanaphon Kallapapruek
        </h4>
        <p className='sm:hidden sm:text-[1rem] px-3 font-bold leading-relaxed'>
          Full-Stack Developer
        </p>
        <div className='hidden sm:flex'>
          <div className='border-r-2  '>
            <p className='sm:text-[1rem] px-3 font-bold leading-relaxed'>
              5 Year Experience
            </p>
          </div>
          <div className='border-r-2  '>
            <p className='sm:text-[1rem] px-3 font-bold leading-relaxed'>
              15+ Projects
            </p>
          </div>
          <div className='  '>
            <p className='sm:text-[1rem] px-3 font-bold leading-relaxed'>
              Mentor of 10+ Students
            </p>
          </div>
        </div>
        <div className='flex justify-center'>
          <a href='Thanaphon-Kallaprapruek-Full-StackEngineer.pdf' download>
            <div className='bg-blue-500 text-white mr-2 p-3 px-4 py-2 mt-5 rounded-[10px] w-fit '>
              <p className='font-bold'>Download CV</p>
            </div>
          </a>
        </div>
      </div>
    </>
  );
};

export default Profile;
