export const siteConfig = {
  title: 'contactus.work',
  description:
    'perfectly crafted for your business, contactus.work',
  url: 'https://contactus.work',
};
