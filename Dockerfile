# Step 1: Base Image
FROM node:18-alpine AS base

# Install pnpm
RUN npm install -g pnpm

# Set working directory
WORKDIR /app

# Install dependencies
COPY pnpm-lock.yaml ./
COPY package.json ./
RUN pnpm install

# Copy application files
COPY . .
COPY public ./public

# Step 2: Build the Application
FROM base AS builder

# Build the Next.js application
RUN pnpm run build

# Step 3: Production Image
FROM node:18-alpine AS production

# Install pnpm
RUN npm install -g pnpm

# Set working directory
WORKDIR /app

# Copy build artifacts from the builder stage
COPY --from=builder /app ./
COPY public ./public

# Install only production dependencies
RUN pnpm install --prod

# Expose the port your app runs on
EXPOSE 3000

# Start the Next.js application
CMD ["pnpm", "start"]